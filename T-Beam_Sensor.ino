#include <lmic.h>
#include <hal/hal.h>
#include <WiFi.h>
#include <SoftwareSerial.h>
#include <Sds011.h>
#include "LoraMessage.h"
#include <Adafruit_ADS1015.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>


// UPDATE the config.h file in the same folder WITH YOUR TTN KEYS AND ADDR.
#include "config.h"

// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.

void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.

void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in *big endian* format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.

void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}


Adafruit_BME280 bme; // I2C


// ADC
Adafruit_ADS1115 ads1115;
//GPS
#include <TinyGPS++.h>
#include <HardwareSerial.h>


#define GPS_TX 12
#define GPS_RX 15

HardwareSerial GPSSerial(1);
TinyGPSPlus tGps;

#define SDS_PIN_RX 13
#define SDS_PIN_TX 14

SoftwareSerial serialSDS (SDS_PIN_RX, SDS_PIN_TX, false, 192);
Sds011Async< SoftwareSerial > sds011(serialSDS);

// T-Beam specific hardware
#undef BUILTIN_LED
#define BUILTIN_LED 21

float uvIndex;

bool is_SDS_running = true;

void start_SDS() {
  Serial.println("Start wakeup SDS011");

  if (sds011.set_sleep(false)) { is_SDS_running = true; }

  Serial.println("End wakeup SDS011");
}

void stop_SDS() {
  Serial.println("Start sleep SDS011");

  if (sds011.set_sleep(true)) { is_SDS_running = false; }

  Serial.println("End sleep SDS011");
}


char s[32]; // used to sprintf for Serial output

static osjob_t sendjob;
// Schedule TX every this many seconds (might become longer due to duty cycle limitations).
const unsigned TX_INTERVAL = 120;

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = 18,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = LMIC_UNUSED_PIN, // was "14,"
  .dio = {26, 33, 32},
};

void onEvent (ev_t ev) {
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      
      // Disable link check validation (automatically enabled
      // during join, but not supported by TTN at this time).
      LMIC_setLinkCheckMode(0);
      break;
    case EV_RFU1:
      Serial.println(F("EV_RFU1"));
      break;
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      digitalWrite(BUILTIN_LED, LOW);
      if (LMIC.txrxFlags & TXRX_ACK) {
        Serial.println(F("Received Ack"));
      }
      if (LMIC.dataLen) {
        sprintf(s, "Received %i bytes of payload", LMIC.dataLen);
        Serial.println(s);
        sprintf(s, "RSSI %d SNR %.1d", LMIC.rssi, LMIC.snr);
        Serial.println(s);
      }
      // Schedule next transmission
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL), do_send);
      break;
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
    default:
      Serial.println(F("Unknown event"));
      break;
  }
}

void do_send(osjob_t* j) {  

  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND)
  {
    Serial.println(F("OP_TXRXPEND, not sending"));
  }
  else
  { 
    int pm25;
    int pm10;
    LoraMessage message;
    // GPS
    while (GPSSerial.available() )
          {
              char data = GPSSerial.read();
              tGps.encode(data);
          }
  
    message.addLatLng(tGps.location.lat(),tGps.location.lng());
    // Feinstaub
    start_SDS();
    delay(60000); // Warte 1min
    sds011.query_data( pm25,pm10, 5);
    Serial.print("PM10: ");
    Serial.println(pm10 / 10.0);
    Serial.print("PM2.5: ");
    Serial.println(pm25 / 10.0);
    message.addTemperature(pm10 / 10.0);
    message.addTemperature(pm25 / 10.0);
    stop_SDS();
    // Temperatur / Luftfeuchte
    message.addTemperature(bme.readTemperature());
    message.addHumidity(bme.readHumidity());
    message.addUint16(int(bme.readPressure() / 100.0F)); // pressure Pa
    Serial.println(bme.readTemperature());
    Serial.println(bme.readHumidity());
    Serial.println(int(bme.readPressure() / 100.0F));
    
    // ADC0 = UV Index
    int16_t adc0, adc1, adc2, adc3;
    
    uint16_t uvIndexValue [12] = { 50, 227, 318, 408, 503, 606, 696, 795, 881, 976, 1079, 1170};
    float scale;
    scale = 6.114/32767;
    adc0 = ads1115.readADC_SingleEnded(0);  // UV an ADC0
    
    float uv = adc0 * scale * 1000.0;
    int i;
    for (i = 0; i < 12; i++) {
        if (uv <= uvIndexValue[i]) {
            uvIndex = i;
            break;
        }
    }
    //calculate 1 decimal if possible
        if (i>0) {
            float vRange=uvIndexValue[i]-uvIndexValue[i-1];
            float vCalc=uv -uvIndexValue[i-1];
            uvIndex+=(1.0/vRange)*vCalc-1.0;
        }
    // restliche ADCs
    adc1 = ads1115.readADC_SingleEnded(1);
    adc2 = ads1115.readADC_SingleEnded(2);
    adc3 = ads1115.readADC_SingleEnded(3);
    Serial.println(F("ADC:"));
    Serial.println(uvIndex);
    Serial.println(adc1 * scale);
    Serial.println(adc2 * scale);
    Serial.println(adc3 * scale);
    // Als Humidity wird gleich float uebertragen. 
    message.addHumidity(uvIndex);
    message.addHumidity(adc1 * scale);
    message.addHumidity(adc2 * scale);
    message.addHumidity(adc3 * scale);
    
    LMIC_setTxData2(1, message.getBytes(), message.getLength(), 0);
    Serial.println(F("Packet queued"));
    digitalWrite(BUILTIN_LED, HIGH);
    }
//    else
    {
      //try again in 3 seconds
//      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(3), do_send);
    }
  // Next TX is scheduled after TX_COMPLETE event.
}

void setup() {
  Serial.begin(115200);
  Serial.println(F("TTN Sensor"));
  
  //Turn off WiFi and Bluetooth
  WiFi.mode(WIFI_OFF);
  btStop();
  serialSDS.begin(9600);
  GPSSerial.begin(9600, SERIAL_8N1, GPS_TX, GPS_RX);
  GPSSerial.setTimeout(2);
  //myHumidity.begin();
  bme.begin();
  ads1115.begin();
  
  Serial.println("SDS011 start/stop and reporting sample");
  String firmware_version;
  uint16_t device_id;
  if (!sds011.device_info(firmware_version, device_id)) {
    Serial.println("Sds011::firmware_version() failed");
  }
  else
  {
    Serial.print("Sds011 firmware version: ");
    Serial.println(firmware_version);
    Serial.print("Sds011 device id: ");
    Serial.println(device_id);
  }

  Sds011::Report_mode report_mode;
  if (!sds011.get_data_reporting_mode(report_mode)) {
    Serial.println("Sds011::get_data_reporting_mode() failed");
  }
  if (Sds011::REPORT_ACTIVE != report_mode) {
    Serial.println("Turning on Sds011::REPORT_ACTIVE reporting mode");
    if (!sds011.set_data_reporting_mode(Sds011::REPORT_ACTIVE)) {
      Serial.println("Sds011::set_data_reporting_mode(Sds011::REPORT_ACTIVE) failed");
    }
  }

  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();
  
  do_send(&sendjob);
  pinMode(BUILTIN_LED, OUTPUT);
  digitalWrite(BUILTIN_LED, LOW);
  
}

void loop() {
    os_runloop_once();
}
